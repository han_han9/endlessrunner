﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterManager : MonoBehaviour {
    public static MonsterManager Instance = null;
    private List<Monster> _monsters = new List<Monster>();

    private void Awake()
    {
        Instance = this;
    }

    public void ReclaimRoad(RoadClip roadClip) {
        roadClip.monsters.ForEach((Monster monster)=> {
            if (monster)
            {
                _monsters.Add(monster);
                monster.gameObject.SetActive(false);
                monster.transform.SetParent(transform);
            }
        });
        roadClip.monsters.Clear();
    }

    public Monster GetMonster(AppMacros.MonsterId id) {

        Monster monster = _monsters.Find((Monster item)=> { return item.id == id; });
        if (monster != null)
        {
            _monsters.Remove(monster);
        }

        return monster;
    }


}
