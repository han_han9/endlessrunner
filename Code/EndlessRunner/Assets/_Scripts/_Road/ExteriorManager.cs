﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExteriorManager: MonoBehaviour {

    public List<GameObject> exteriors;

    public static ExteriorManager Instance = null;

    private void Awake()
    {
        Instance = this;
    }

    public GameObject GetExterior(int exteriorIndex) {
        if (exteriorIndex < exteriors.Count)
        {
            return exteriors[exteriorIndex];
        }
        else {
            return null;
        }
    }



}
