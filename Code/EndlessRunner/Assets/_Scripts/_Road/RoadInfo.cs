﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RoadInfo {
    
    [System.Serializable]
    public class MonsterInfo {
        public Vector3 position = Vector3.zero;
        public Vector3 rotation = Vector3.zero;
        public Vector3 scale = Vector3.one;
        public AppMacros.MonsterId id = AppMacros.MonsterId.MonsterMouse;
    }

    [System.Serializable]
    public class ObstacleInfo {
        public Vector3 position = Vector3.zero;
        public Vector3 rotation = Vector3.zero;
        public Vector3 scale = Vector3.one;
        public AppMacros.ObstacleId id = AppMacros.ObstacleId.ObstacleStone;
    }

    [System.Serializable]
    public class PropInfo {
        public Vector3 position = Vector3.zero;
        public Vector3 rotation = Vector3.zero;
        public Vector3 scale = Vector3.one;
        public AppMacros.PropId id = AppMacros.PropId.PropCoin;
    }
    
    public List<MonsterInfo> monsterInfos;
    
    public List<ObstacleInfo> obstacleInfos;
    
    public List<PropInfo> propInfos;

    public string roadName;

}
