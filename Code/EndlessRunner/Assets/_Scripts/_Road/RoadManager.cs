﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadManager : MonoBehaviour {

    public static RoadManager Instance = null;
    public RoadList roadList;
    public RoadConfigSpawner roadConfigSpawner;

    private List<RoadClip> _roadPool;
    private List<RoadClip> _showRoads;

    private void Awake()
    {
        Instance = this;
        _showRoads = new List<RoadClip>();
        _roadPool = new List<RoadClip>();
    }

    /// <summary>
    /// 第一次 初始化赛道
    /// Shows the road.
    /// </summary>
    public void ShowRoad() {

        var startPos = Vector3.zero;
        for (int i = 0; i < 10; i++)
        {
            NextRoad();
        }
    }

    public void NextRoad() {
        int roadIndex = Random.Range(0, roadConfigSpawner.roadInfos.Count);
        RoadInfo roadInfo = roadConfigSpawner.roadInfos[roadIndex];
        RoadClip road = _roadPool.Find((RoadClip item) => { return item.name == roadInfo.roadName && !item.gameObject.activeSelf; });
        if (road == null)
        {
            RoadClip roadClip = roadList.roads.Find((RoadClip item) => { return item.gameObject.name == roadInfo.roadName; });
            road = Instantiate(roadClip);
            road.gameObject.name = roadInfo.roadName;
            road.transform.SetParent(transform);
        }
        else {
            _roadPool.Remove(road);
        }
        if (_showRoads.Count == 0)
        {
            road.transform.localPosition = Vector3.zero;
        }
        else
        {
            road.transform.localPosition = _showRoads[_showRoads.Count - 1].transform.localPosition + new Vector3(0, 0, _showRoads[_showRoads.Count - 1].dis / 2 + road.dis / 2);
        }
        _showRoads.Add(road);
        road.gameObject.SetActive(true);
        road.SerializeRoadInfo(roadInfo);
    }


    public void ReclaimRoad(RoadClip roadClip) {
        _showRoads.Remove(roadClip);
        _roadPool.Add(roadClip);
        roadClip.gameObject.SetActive(false);
        ObstacleManager.Instance.ReclaimRoad(roadClip);
        MonsterManager.Instance.ReclaimRoad(roadClip);
        PropManager.Instance.ReclaimRoad(roadClip);
    }


}
