﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prop : MonoBehaviour {

    public AppMacros.PropId id;


    private void OnTriggerEnter(Collider other) {
        Debug.Log(other);
        if (other.tag == "Character") {
            CollisionCharacter();
        }
    }

    private void CollisionCharacter() {
        gameObject.SetActive(false);

    }


}
