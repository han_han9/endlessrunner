﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadClip : MonoBehaviour {

    [HideInInspector]
    public List<Monster> monsters;
    
    [HideInInspector]
    public List<Obstacle> obstacles;

    [HideInInspector]
    public List<Prop> props;


    public Transform startTrans;
    public Transform endTrans;

    public float dis;

    private void Awake()
    {
        monsters = new List<Monster>();
        obstacles = new List<Obstacle>();
        props = new List<Prop>();
    }

    public void SerializeRoadInfo(RoadInfo infos) {

        SerializeMonster(infos.monsterInfos);
        SerializeObstacle(infos.obstacleInfos);
        SerializeProp(infos.propInfos);
    }

    public void SerializeMonster(List<RoadInfo.MonsterInfo> monsterInfos) {
        
    }

    public void SerializeObstacle(List<RoadInfo.ObstacleInfo> obstacleInfos) {
        obstacleInfos.ForEach((RoadInfo.ObstacleInfo obstacleInfo)=> {
            Obstacle obstacle = ObstacleManager.Instance.GetObstacle(obstacleInfo.id);
            obstacle.transform.parent = transform;
            obstacle.transform.localPosition = obstacleInfo.position;
            obstacle.transform.localRotation = Quaternion.Euler(obstacleInfo.rotation);
            obstacles.Add(obstacle);
        });
    }

    public void SerializeProp(List<RoadInfo.PropInfo> propInfos) {
        propInfos.ForEach((RoadInfo.PropInfo propInfo)=> {
            Prop prop = PropManager.Instance.GetProp(propInfo.id);
            prop.transform.parent = transform;
            prop.transform.localPosition = propInfo.position;
            prop.transform.localRotation = Quaternion.Euler(propInfo.rotation);
            props.Add(prop);
        });
    }




    private void Update()
    {
        //if (endTrans.position.z < CharacterControl.Instance.pos.z)
        //{
        //    RoadManager.Instance.ReclaimRoad(this);
        //}
    }






}
