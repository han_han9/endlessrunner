﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour {

    public static ObstacleManager Instance = null;
    public List<Obstacle> obstaclesList;

    private List<Obstacle> _obstaclePool = new List<Obstacle>();
    

    private void Awake()
    {
        Instance = this;
    }

    public void ReclaimRoad(RoadClip roadClip)
    {
        roadClip.obstacles.ForEach((Obstacle obstacle)=> {
            if (obstacle)
            {
                _obstaclePool.Add(obstacle);
                obstacle.gameObject.SetActive(false);
                obstacle.transform.SetParent(transform);
            }
        });
        roadClip.obstacles.Clear();
    }

    public Obstacle GetObstacle(AppMacros.ObstacleId id) {
        Obstacle obstacle = _obstaclePool.Find((Obstacle item)=> { return item.id == id; });
        if (obstacle) {
            _obstaclePool.Remove(obstacle);
        } else {
            Obstacle obstaclePrefab = obstaclesList.Find((Obstacle item)=> {
                return item.id == id;
            });
            obstacle = Instantiate(obstaclePrefab);
        }
        return obstacle;
    }

}
