﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropManager : MonoBehaviour {

    public static PropManager Instance = null;
    public List<Prop> propsList;


    private List<Prop> propPool = new List<Prop>();

    private void Awake()
    {
        Instance = this;
    }

    public void ReclaimRoad(RoadClip roadClip) {
        roadClip.props.ForEach((Prop prop)=> {
            if (prop)
            {
                propPool.Add(prop);
                prop.gameObject.SetActive(false);
                prop.transform.SetParent(transform);
            }
        });
        roadClip.props.Clear();
    }

    public Prop GetProp(AppMacros.PropId id) {
        Prop prop = propPool.Find((Prop item) => { return id == item.id; });
        if (prop) {
            propPool.Remove(prop);
        } else {
            Prop propPrefab = propsList.Find((Prop item) => {
                return item.id == id;
            });
            prop = Instantiate(propPrefab);
        }
        return prop;
    }



}
