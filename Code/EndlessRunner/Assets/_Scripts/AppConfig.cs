﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppConfig : MonoBehaviour {


    public static AppConfig Instance = null;

    public float gravity = 45.0f;

    private void Awake() {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
    
}
