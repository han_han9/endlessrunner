﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControl : MonoBehaviour {


    public static CharacterControl Instance;
    
    public Animator animator;

    public Character character;
    

    [HideInInspector]
    public Vector3 pos = Vector3.zero;

    [HideInInspector]
    public Vector3 targetPos = Vector3.zero;
    
    public float moveSpeed = 10;

    [HideInInspector]
    public int roadIndex = 0;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        //FSM.Instance.ChangeState<RunState>();
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.S))
        {
            FSM.Instance.ChangeState<SlideState>();
        }
        else if (Input.GetKeyUp(KeyCode.W))
        {
            FSM.Instance.ChangeState<JumpState>();
        } else if (Input.GetKeyUp(KeyCode.A)) {
            if (roadIndex > -1) {
                FSM.Instance.ChangeState<TurnState>(-1);
            }
        } else if (Input.GetKeyUp(KeyCode.D)) {
            if (roadIndex < 1) {
                FSM.Instance.ChangeState<TurnState>(1);
            }
        }
        UpdatePosition();
    }

    void UpdatePosition() {
        transform.localPosition = targetPos;
    }







}
