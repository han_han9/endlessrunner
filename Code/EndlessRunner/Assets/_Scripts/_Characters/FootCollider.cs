﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootCollider : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other) {
        if (other.tag == "Ground") {
            Debug.Log("OnTriggerEnter:" + FSM.Instance.currentState);
            if (FSM.Instance && FSM.Instance.currentState is FallState) {
                Debug.Log(other);
                FSM.Instance.ChangeState<RunState>();
            }
        }
    }
}
