﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM : MonoBehaviour {

    public static FSM Instance = null;
    [HideInInspector]
    public FSMState currentState = null;
    [HideInInspector]
    public FSMState lastState = null;
    private bool inTranslation = false;

    private void Awake()
    {
        Instance = this;
    }


    public T GetState<T>() where T : FSMState {
        T target = GetComponent<T>();
        if (target == null)
        {
            target = gameObject.AddComponent<T>();
        }
        return target;
    }

    public void ChangeState(FSMState state, object param = null) {
        Translation(state, param);
    }

    public void ChangeState<T>(object param = null) where T : FSMState {
        Translation(GetState<T>(), param);
    }


    private void Translation(FSMState state, object param = null){
        if (currentState != null && !state.CheckState(currentState) || inTranslation || state == currentState)
        {
            return;
        }

        inTranslation = true;

        if (currentState) {
            currentState.OnExit();
        }

        lastState = currentState;
        currentState = state;
        if (currentState != null)
        {
            currentState.OnEnter(param);
        }
        inTranslation = false;

    }

}
