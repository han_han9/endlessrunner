﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSMState : MonoBehaviour {




    protected object _param = null;

    /// <summary>
    /// 当返回false 的时候， 状态不切换
    /// Checks the state.
    /// </summary>
    /// <returns><c>true</c>, if state was checked, <c>false</c> otherwise.</returns>
    /// <param name="lastState">Last state.</param>
    public virtual bool CheckState(FSMState lastState) {
        return true;
    }


    public virtual void OnEnter(object _param) {
        this._param = _param;
    }

    public virtual void OnExit() {

    }


}
