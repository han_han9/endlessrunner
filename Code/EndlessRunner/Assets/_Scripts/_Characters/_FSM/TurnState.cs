﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnState : FSMState {



    private Coroutine turnCoroutine = null;
    private float turnSpeed = 20.0f;
    private float fallSpeed = 0;

    public override bool CheckState(FSMState lastState) {
        return true;
    }

    public override void OnEnter(object _param) {
        base.OnEnter(_param);
        if ((int)_param == 1) {
            CharacterControl.Instance.animator.SetInteger("Turning", 1);
        } else if ((int)_param == -1) {
            CharacterControl.Instance.animator.SetInteger("Turning", -1);
        }
        fallSpeed = 0;
        turnCoroutine = StartCoroutine(TurnCoroutine());

        
    }

    public override void OnExit() {
        if (turnCoroutine != null) {
            StopCoroutine(turnCoroutine);
            turnCoroutine = null;
        }
        CharacterControl.Instance.animator.SetInteger("Turning", 0);
        base.OnExit();
    }

    public IEnumerator TurnCoroutine() {
        int targetRoadIndex = CharacterControl.Instance.roadIndex + (int)_param;
        Vector3 targetPos = CharacterControl.Instance.targetPos;
        float offsetY = 0;
        if ((int)_param < 0) {
            
            do {
                targetPos.x -= turnSpeed * Time.deltaTime;
                targetPos.z += CharacterControl.Instance.moveSpeed * Time.deltaTime;
                offsetY = CorrectHeight();
                if (offsetY < targetPos.y) {
                    fallSpeed += AppConfig.Instance.gravity * Time.deltaTime;
                    targetPos.y -= fallSpeed * Time.deltaTime;
                }
                CharacterControl.Instance.targetPos = targetPos;
                yield return null;
            } while (targetPos.x > targetRoadIndex * 4);
        } else if ((int)_param > 0) {
            do {
                targetPos.x += turnSpeed * Time.deltaTime;
                targetPos.z += CharacterControl.Instance.moveSpeed * Time.deltaTime;
                offsetY = CorrectHeight();
                if (offsetY < targetPos.y) {
                    targetPos.y -= 10 * Time.deltaTime;
                }
                CharacterControl.Instance.targetPos = targetPos;
                yield return null;
            } while (targetPos.x < targetRoadIndex * 4);
        }
        targetPos.x = targetRoadIndex * 4;
        CharacterControl.Instance.targetPos = targetPos;
        CharacterControl.Instance.roadIndex = targetRoadIndex;
        offsetY = CorrectHeight();
        if (offsetY < (targetPos.y - 0.2)) {
            FSM.Instance.ChangeState<FallState>();
        } else {
            FSM.Instance.ChangeState<RunState>();
        }
        
    }

    /// <summary>
    /// 修正跑的过程中 由于碰撞照成的误差
    /// </summary>
    private float CorrectHeight() {
        Ray ray = new Ray(transform.position + Vector3.up * 3, -transform.up);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Ground"))) {
            return hit.point.y;
        }
        return -1;
    }


}
