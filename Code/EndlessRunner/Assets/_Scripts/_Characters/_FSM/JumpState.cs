﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpState : FSMState {




    private Coroutine jumpCoroutine = null;

    private float jumpMultiplier = 1.2f;
    private float jumpSpeed = 5.0f;

    public override bool CheckState(FSMState lastState)
    {

        if (lastState is RunState) {
            return true;
        }
        return false;
    }

    public override void OnEnter(object _param)
    {
        CharacterControl.Instance.animator.SetBool("Jumping", true);
        jumpSpeed = AppConfig.Instance.gravity * 0.5f * 0.833f;
        jumpCoroutine = StartCoroutine(JumpCoroutine());
        base.OnEnter(_param);
    }

    public override void OnExit()
    {
        if (jumpCoroutine != null)
        {
            StopCoroutine(jumpCoroutine);
            jumpCoroutine = null;
        }
        CharacterControl.Instance.animator.SetBool("Jumping", false);
        base.OnExit();
    }

    public IEnumerator JumpCoroutine() {
        while (jumpSpeed > 0)
        {
            Vector3 targetPos = CharacterControl.Instance.targetPos;
            targetPos.z += CharacterControl.Instance.moveSpeed * jumpMultiplier * Time.deltaTime;
            targetPos.y += jumpSpeed * Time.deltaTime;
            CharacterControl.Instance.targetPos = targetPos;
            jumpSpeed -= AppConfig.Instance.gravity * Time.deltaTime;
            yield return null;
        }
        FSM.Instance.ChangeState<FallState>();

    }

}
