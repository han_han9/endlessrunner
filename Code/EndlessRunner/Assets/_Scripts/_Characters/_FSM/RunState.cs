﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunState : FSMState {


    private Coroutine runCoroutine = null;

    public override bool CheckState(FSMState lastState)
    {
        if (!base.CheckState(lastState))
        {
            return false;
        }
        return true;
    }

    public override void OnEnter(object _param)
    {
        Debug.Log("Enter Run");
        CharacterCamera.Instance.SetCameraState(AppMacros.CameraState.CameraRun);
        CharacterControl.Instance.animator.SetBool("Running", true);
        runCoroutine = StartCoroutine(RunCoroutine());
        base.OnEnter(_param);
    }

    public override void OnExit()
    {
        CharacterControl.Instance.animator.SetBool("Running", false);
        if (runCoroutine != null)
        {
            StopCoroutine(runCoroutine);
            runCoroutine = null;
        }
        base.OnExit();
    }

    public IEnumerator RunCoroutine() {
        Vector3 targetPos = CharacterControl.Instance.targetPos;
        while (true) {
            targetPos.z += CharacterControl.Instance.moveSpeed * Time.deltaTime;
            float offsetY = CorrectHeight();
            if (offsetY > targetPos.y) {
                targetPos.y = offsetY;
            } else if (offsetY < (targetPos.y - 1)) {
                FSM.Instance.ChangeState<FallState>();
            }
            
            CharacterControl.Instance.targetPos = targetPos;
            //CorrectHeight();
            yield return null;
        }
    }

    /// <summary>
    /// 修正跑的过程中 由于碰撞照成的误差
    /// </summary>
    private float CorrectHeight() {
        Ray ray = new Ray(transform.position + Vector3.up*3, -transform.up);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, 1 << LayerMask.NameToLayer("Ground"))) {
            return hit.point.y;
        }
        return -1;
    }


}
