﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DieState : FSMState {

    public override bool CheckState(FSMState lastState) {
        return base.CheckState(lastState);
    }

    public override void OnEnter(object _param) {
        CharacterControl.Instance.animator.SetBool("Dying", true);
        base.OnEnter(_param);
    }

    public override void OnExit() {

        base.OnExit();
    }


}
