﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallState : FSMState {


    private Coroutine fallCoroutine = null;
    private float fallMultiplier = 1.2f;
    private float fallSpeed = 3.0f;
    public override bool CheckState(FSMState lastState)
    {
        if (!base.CheckState(lastState))
        {
            return false;
        }
        return true;
    }

    public override void OnEnter(object _param)
    {
        Debug.Log("Fall");
        fallSpeed = 0;
        fallCoroutine = StartCoroutine(FallCoroutine());
        CharacterCamera.Instance.SetCameraState(AppMacros.CameraState.CameraFall);
        CharacterControl.Instance.animator.SetBool("Falling", true);
        base.OnEnter(_param);
    }

    public override void OnExit()
    {
        if (fallCoroutine != null) {
            StopCoroutine(fallCoroutine);
            fallCoroutine = null;
        }

        CharacterControl.Instance.animator.SetBool("Falling", false);
        base.OnExit();
    }

    public IEnumerator FallCoroutine() {
        while (true) {
            Vector3 targetPos = CharacterControl.Instance.targetPos;
            targetPos.z += CharacterControl.Instance.moveSpeed * fallMultiplier * Time.deltaTime;
            targetPos.y -= fallSpeed * Time.deltaTime;
            fallSpeed += AppConfig.Instance.gravity * Time.deltaTime;
            CharacterControl.Instance.targetPos = targetPos;
            yield return null;
        }
    }


}
