﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideState : FSMState {


    private Coroutine slideCoroutine = null;

    private float slideMultiplier = 0.8f;

    public override bool CheckState(FSMState lastState)
    {
        if (lastState is RunState) {
            return true;
        }
        return true;
    }


    public override void OnEnter(object _param)
    {
        slideCoroutine = StartCoroutine(SlideCoroutine());
        CharacterControl.Instance.animator.SetBool("Sliding", true);
        base.OnEnter(_param);
    }

    public override void OnExit()
    {
        if (slideCoroutine != null)
        {   
            StopCoroutine(slideCoroutine);
        }
        CharacterControl.Instance.animator.SetBool("Sliding", false);
        base.OnExit();
    }

    public IEnumerator SlideCoroutine() {
        while (true)
        {
            CharacterControl.Instance.targetPos += Vector3.forward * CharacterControl.Instance.moveSpeed * slideMultiplier * Time.deltaTime;
            yield return null;
        }
    }



}
