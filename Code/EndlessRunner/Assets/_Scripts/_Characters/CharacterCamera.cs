﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCamera : MonoBehaviour {



    public static CharacterCamera Instance = null;
    public Transform followTarget = null;

    private Vector3 offsetPos;
    private Vector3 offsetRotation;

    private void Awake() {
        Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.Lerp(transform.position, followTarget.position + offsetPos, Time.deltaTime * 10);
        Quaternion rotation = Quaternion.Euler(offsetRotation);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 2f);
    }

    public void SetCameraState(AppMacros.CameraState state) {
        switch (state) {
            case AppMacros.CameraState.CameraIdle:
                break;
            case AppMacros.CameraState.CameraRun:
                offsetPos = new Vector3(0, 5, -6);
                offsetRotation = new Vector3(20, 0, 0);
                break;
            case AppMacros.CameraState.CameraJump:
                break;
            case AppMacros.CameraState.CameraLeft:
                break;
            case AppMacros.CameraState.CameraRight:
                break;
            case AppMacros.CameraState.CameraFall:
                offsetPos = new Vector3(0, 6, -6);
                offsetRotation = new Vector3(30, 0, 0);
                break;
            default:
                break;
        }
    }

    


}
