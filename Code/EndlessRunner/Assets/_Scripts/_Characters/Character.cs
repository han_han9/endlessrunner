﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    	
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Ground"))
        {
            Debug.Log(other);

            if (FSM.Instance && FSM.Instance.currentState is FallState) {
                FSM.Instance.ChangeState<RunState>();
            }
        } else if (other.gameObject.layer == LayerMask.NameToLayer("Obstacle")) {
            FSM.Instance.ChangeState<DieState>();
        }
    }

}
