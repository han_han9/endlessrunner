﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/RoadConfigSpawner")]
public class RoadConfigSpawner : ScriptableObject {
    public List<RoadInfo> roadInfos = new List<RoadInfo>();


    public void UpdateRoadSpawnerInfo(string roadName, List<RoadInfo.MonsterInfo> monsterInfos, List<RoadInfo.ObstacleInfo> obstacleInfos, List<RoadInfo.PropInfo> propInfos) {
        RoadInfo roadInfo = roadInfos.Find((RoadInfo item)=> { return item.roadName == roadName; });
        if (roadInfo == null)
        {
            roadInfo = new RoadInfo();
            roadInfo.roadName = roadName;
            roadInfo.monsterInfos = monsterInfos;
            roadInfo.obstacleInfos = obstacleInfos;
            roadInfo.propInfos = propInfos;
            roadInfos.Add(roadInfo);
        }
        else {
            roadInfo.monsterInfos = monsterInfos;
            roadInfo.obstacleInfos = obstacleInfos;
            roadInfo.propInfos = propInfos;
        }


    }

}
