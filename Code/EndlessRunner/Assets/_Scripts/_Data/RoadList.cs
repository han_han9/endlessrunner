﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/RoadList")]
public class RoadList : ScriptableObject {
    public List<RoadClip> roads = new List<RoadClip>();


    public void UpdateRoadClip(RoadClip roadClip) {
        Debug.Log(roadClip.name);
        var _roadClip = roads.Find((RoadClip item)=> { return item == null ? false : item.gameObject.name == roadClip.gameObject.name; });
        if (_roadClip == null)
        {
            bool isInsert = false;
            for (int i = 0; i < roads.Count; i++)
            {
                if (roads[i] == null)
                {
                    roads[i] = roadClip;
                    isInsert = true;
                    break;
                }
            }
            if (!isInsert)
            {
                roads.Add(roadClip);
            }
            
        }
    }

}
