﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MainUI : UILayer {

    public Image splash;

    public override void OnEnter(object param) {
        base.OnEnter(param);
        StartCoroutine(HideSplash());
        //Sche
    }

    public void OnLisenter() {
        UIControl.Instance.ShowLayer(AppMacros.LayerId.GameLayer);
    }

    private IEnumerator HideSplash() {
        yield return new WaitForSeconds(1.0f);
        //splash.gameObject.SetActive(false);
        splash.DOFade(0, 1.0f);
    }

}
