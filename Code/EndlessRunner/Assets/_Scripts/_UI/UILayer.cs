﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILayer: MonoBehaviour, IBaseUI {
    
    public AppMacros.LayerId id;

    private object _param = null;

    public virtual void OnEnter(object param)
    {
        _param = param;
        gameObject.SetActive(true);
        OnRefresh();
    }

    public virtual void OnExit()
    {
        gameObject.SetActive(false);
    }

    public virtual void OnRefresh()
    {

    }
}
