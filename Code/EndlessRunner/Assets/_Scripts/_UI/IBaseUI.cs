﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBaseUI {
    
    void OnEnter(object param);
    void OnRefresh();
    void OnExit();
}
