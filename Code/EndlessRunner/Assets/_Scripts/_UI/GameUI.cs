﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameUI : UILayer {

    public Text debugText = null;

    private float lastDis = 0;

    public override void OnEnter(object param)
    {

        

        base.OnEnter(param);


    }

    public override void OnRefresh()
    {
        StartCoroutine(LoadGameScene());
        base.OnRefresh();
    }


    private void Update() {
        if (CharacterControl.Instance) {
            debugText.text = ((CharacterControl.Instance.transform.position.z - lastDis) / Time.deltaTime).ToString();
            lastDis = CharacterControl.Instance.transform.position.z;
        }

    }

    IEnumerator LoadGameScene() {
        yield return SceneManager.LoadSceneAsync("Game");
        RoadManager.Instance.ShowRoad();
        CharacterControl.Instance.animator.SetTrigger("StartRun");
        FSM.Instance.ChangeState<RunState>();
    }

}
