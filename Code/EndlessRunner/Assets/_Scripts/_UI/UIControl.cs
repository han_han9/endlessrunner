﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AppMacros;

public class UIControl : MonoBehaviour {

    // Use this for initialization

    public static UIControl Instance = null;

    [SerializeField]
    List<UILayer> layers;

    [SerializeField]
    List<UIDlg> dlgs;

    private UILayer _curLayer = null;

    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    void Start () {
		
	}


    public void ShowLayer(LayerId id, object param = null) {
        if (_curLayer)
        {
            _curLayer.OnExit();
        }
        _curLayer = layers.Find((UILayer layer) => { return layer.id == id; });
        if (_curLayer != null)
        {
            _curLayer.OnEnter(param);
        }
        else {
            Debug.Log("Don't Find Layer:" + id);
        }
    }


    public void ShowDlg(DlgId id, object param) {

    }


    


}
