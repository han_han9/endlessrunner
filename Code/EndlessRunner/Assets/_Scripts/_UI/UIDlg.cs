﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIDlg : MonoBehaviour, IBaseUI {

    public AppMacros.DlgId id;

    public virtual void OnEnter(object param)
    {

    }

    public virtual void OnExit()
    {

    }

    public virtual void OnRefresh()
    {

    }

}
