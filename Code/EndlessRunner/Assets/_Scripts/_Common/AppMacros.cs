﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AppMacros
{
    
    public enum LayerId {
        MainLayer,
        GameLayer

    }

    public enum DlgId {
        ResultDlg
    }

    public enum MonsterId {
        MonsterMouse
    }

    public enum ObstacleId {
        ObstacleStone
    }

    public enum PropId {
        PropCoin
    }

    public enum CameraState {
        CameraIdle, // 待机
        CameraRun,  // 跑
        CameraJump, // 跳
        CameraLeft, // 左边跑道
        CameraRight, // 右边跑道
        CameraFall, // 下落
    }



}
