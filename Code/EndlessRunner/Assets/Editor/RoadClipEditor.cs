﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(RoadClip))]
public class RoadClipEditor : Editor {


    private void SyncApply(GameObject targetGameObject) {
        string r_GameObjectName = "r_" + targetGameObject.name;

        GameObject roadClipCopy = Instantiate(targetGameObject, Vector3.zero, targetGameObject.transform.rotation) as GameObject;
        roadClipCopy.transform.parent = null;
        PrefabUtility.DisconnectPrefabInstance(roadClipCopy);
        roadClipCopy.name = r_GameObjectName;

        SyncSavaData(roadClipCopy);

        string runtimeRoadClipPath = "Assets/_RuntimeRoadClips/" + r_GameObjectName + ".prefab";
        GameObject prefabGameObject = null;
        Object prefabObj = AssetDatabase.LoadAssetAtPath(runtimeRoadClipPath, typeof(Object));
        if (prefabObj == null)
        {
            prefabGameObject = PrefabUtility.CreatePrefab(runtimeRoadClipPath, roadClipCopy);
        }
        else
        {
            prefabGameObject = PrefabUtility.ReplacePrefab(roadClipCopy, prefabObj, ReplacePrefabOptions.ReplaceNameBased);
        }

        if (prefabGameObject)
        {
            string roadListAssetPath = "Assets/_Data/RoadList.asset";
            RoadList roadList = AssetDatabase.LoadAssetAtPath(roadListAssetPath, typeof(RoadList)) as RoadList;
            roadList.UpdateRoadClip(prefabGameObject.GetComponent<RoadClip>());
        }


        DestroyImmediate(roadClipCopy);
        AssetDatabase.SaveAssets();
    }

    public void SyncSavaData(GameObject targetGameObject) {
        string configSpawnerAssetPath = "Assets/_Data/RoadConfigSpawner.asset";
        RoadConfigSpawner roadConfigSpawner = AssetDatabase.LoadAssetAtPath(configSpawnerAssetPath, typeof(RoadConfigSpawner)) as RoadConfigSpawner;
        List<RoadInfo.PropInfo> propInfos = new List<RoadInfo.PropInfo>();
        foreach (var prop in targetGameObject.GetComponentsInChildren<Prop>())
        {
            RoadInfo.PropInfo propInfo = new RoadInfo.PropInfo
            {
                id = prop.id,
                position = prop.transform.localPosition,
                rotation = prop.transform.localRotation.eulerAngles,
                scale = prop.transform.localScale
            };
            DestroyImmediate(prop.gameObject);
            propInfos.Add(propInfo);
        }

        List<RoadInfo.MonsterInfo> monsterInfos = new List<RoadInfo.MonsterInfo>();
        foreach (var monster in targetGameObject.GetComponentsInChildren<Monster>())
        {
            RoadInfo.MonsterInfo monsterInfo = new RoadInfo.MonsterInfo {
                id = monster.id,
                position = monster.transform.localPosition,
                rotation = monster.transform.localRotation.eulerAngles,
                scale = monster.transform.localScale
            };
            DestroyImmediate(monster.gameObject);
            monsterInfos.Add(monsterInfo);
        }

        List<RoadInfo.ObstacleInfo> obstacleInfos = new List<RoadInfo.ObstacleInfo>();
        foreach (var obstacle in targetGameObject.GetComponentsInChildren<Obstacle>())
        {
            RoadInfo.ObstacleInfo obstacleInfo = new RoadInfo.ObstacleInfo {
                id = obstacle.id,
                position = obstacle.transform.localPosition,
                rotation = obstacle.transform.localRotation.eulerAngles,
                scale = obstacle.transform.localScale
            };
            DestroyImmediate(obstacle.gameObject);
            obstacleInfos.Add(obstacleInfo);
        }
        roadConfigSpawner.UpdateRoadSpawnerInfo(targetGameObject.name, monsterInfos, obstacleInfos, propInfos);


    }


    public override void OnInspectorGUI()
    {
        GameObject targetGameObject = (target as Component).gameObject;
        if (!targetGameObject.name.StartsWith("r_"))
        {
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Sync to Runtime"))
            {
                AssetDatabase.SaveAssets();
                SyncApply(targetGameObject);
            }
            EditorGUILayout.EndHorizontal();
        }

        base.OnInspectorGUI();
    }

}
